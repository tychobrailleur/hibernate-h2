package com.example.hibh2.service;


import com.example.hibh2.model.Employee;
import com.example.hibh2.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @PostConstruct
    public void processEmployees() {

        Employee employee = new Employee();
        employee.setName("Michael Scott");

        Employee emp = employeeRepository.save(employee);
        System.out.println(emp.getId());

    }
}
